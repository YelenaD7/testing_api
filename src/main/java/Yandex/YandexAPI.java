package Yandex;

import com.google.gson.Gson;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONException;
import kong.unirest.json.JSONObject;
import lombok.SneakyThrows;

public class YandexAPI {
    private static final String URL = "https://translate.yandex.net/api/v1.5/tr.json/translate";
    private static final String KEY = "pAQAAAAAg9q-kAATuwToLaI2byk2foJ3CgseZlys";

    @SneakyThrows
    public String translate(String lang, String text) {
        Gson gson = new Gson();

        HttpResponse<JsonNode> response;
        response = Unirest.post(URL)

                .header("accept", "application/json")
                .queryString("key", KEY)
                .queryString("lang", lang)
                .body(text)
                .asJson();

        return response.getBody().getObject().getString("text");

    }


}
