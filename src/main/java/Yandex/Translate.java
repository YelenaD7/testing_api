package Yandex;

import com.google.gson.annotations.SerializedName;

public class Translate {
    @SerializedName("key")
    public String key;
    @SerializedName("lang")
    public String lang;

    public Translate(String key, String lang) {
        this.key = key;
        this.lang = lang;
    }


    public String getKey() {
        return key;
    }

    public String getLang() {
        return lang;
    }
}
