import Yandex.Translate;
import Yandex.YandexAPI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TransferQueue;

public class YandexTesting {
    private static final String RussianText = "Всем привет!";
    private static final String EnglishText = "Hello World!";


    @Test
    @DisplayName("Переводчик")
    public void getTranslate() {
        YandexAPI yandexAPI = new YandexAPI();
        String translate = yandexAPI.translate("en-ru", EnglishText);
        Assertions.assertEquals(translate, RussianText);

    }
}
